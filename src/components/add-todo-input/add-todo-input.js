import React from "react";

import "./add-todo-input.css";

class AddTodo extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        if (this.label.value) {
            this.props.onAdd(this.label.value);
            this.label.value = "";
        }
    };
    render() {
        return (
            <form className="add-todo-form" onSubmit={this.handleSubmit}>
                <input
                    type="text"
                    ref={(input) => {
                        this.label = input;
                    }}
                />
                <input type="submit" value="Add" />
            </form>
        );
    }
}

export default AddTodo;
