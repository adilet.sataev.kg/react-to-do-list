import React, { useState } from "react";

import "./search-panel.css";

const SearchPanel = ({ onSearch }) => {
    const [state, setState] = useState({ search: "" });

    const handleChange = (e) => {
        setState({ search: e.target.value });
        onSearch(e.target.value)
    };

    return (
        <input
            type="text"
            className="form-control search-input"
            placeholder="type to search"
            value={state.search}
            onChange={handleChange}
        />
    );
};

export default SearchPanel;
