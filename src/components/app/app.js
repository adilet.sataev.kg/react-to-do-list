import React from "react";

import AppHeader from "../app-header";
import SearchPanel from "../search-panel";
import TodoList from "../todo-list";
import ItemStatusFilter from "../item-status-filter";
import AddTodo from "../add-todo-input";

import "./app.css";

class App extends React.Component {
    state = {
        data: [
            { label: "Drink Coffee", important: false, id: 1, done: true },
            { label: "Make Awesome App", important: true, id: 2, done: false },
            { label: "Have a lunch", important: false, id: 3, done: false },
        ],
        search: "",
        filter: "all",
    };

    handleToggleImportant = (id) => {
        this.setState((prevState) => {
            const newData = [...prevState.data]
            const index = newData.findIndex((el) => el.id === id);
            newData[index].important = !newData[index].important
            return {data:newData};
        });
    };

    handleDelete = (id) => {
        this.setState((prevState) => {
            const newData = [...prevState.data]
            const index = newData.findIndex((el) => el.id === id);
            newData.splice(index, 1)
            return {data:newData};
        });
    };

    handleToggleDone = (id) => {
        this.setState((prevState) => {
            const newData = [...prevState.data]
            const index = newData.findIndex((el) => el.id === id);
            newData[index].done = !newData[index].done
            return {data:newData};
        });
    };

    handleSearch = (search) => {
        this.setState((prevState) => {
            return { search: search.toLowerCase() };
        });
    };

    handleFilterChange = (e) => {
        this.setState((prevState) => {
            return { filter: e.target.innerText.toLowerCase() };
        });
    };

    handleAdd = (label) => {
        const newTodo = {
            id: new Date(),
            label: label,
            important: false,
            done: false,
        };
        this.setState((prevState) => {
            return {
                data: [...prevState.data, newTodo],
            };
        });
    };

    render() {
        const filteredTodos = this.state.data.filter((todo) => {
            if (this.state.filter === "all") {
                return true;
            } else if (this.state.filter === "active") {
                return !todo.done;
            } else {
                return todo.done;
            }
        });

        const searchingTodos = filteredTodos.filter((todo) => {
            return todo.label.toLowerCase().includes(this.state.search);
        });

        const done = this.state.data.filter((todo) => {
            return todo.done;
        }).length;
        const toDo = this.state.data.length - done;

        return (
            <div className="todo-app">
                <AppHeader toDo={toDo} done={done} />
                <div className="top-panel d-flex">
                    <SearchPanel onSearch={this.handleSearch} />
                    <ItemStatusFilter
                        onFilterChange={this.handleFilterChange}
                    />
                </div>

                <TodoList
                    todos={searchingTodos}
                    onToggleDone={this.handleToggleDone}
                    onToggleImportant={this.handleToggleImportant}
                    onDelete={this.handleDelete}
                />
                <AddTodo onAdd={this.handleAdd} />
            </div>
        );
    }
}

export default App;
